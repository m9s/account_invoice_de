# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelWorkflow, ModelView, ModelSQL, fields
from trytond.modules.account_invoice.invoice import _STATES

class Invoice(ModelWorkflow, ModelSQL, ModelView):
    _name = 'account.invoice'

    def __init__(self):
        super(Invoice, self).__init__()
        self._sql_constraints += [
            ('external_invoice_key_uniq',
                'UNIQUE(external_invoice_key)',
                'The external invoice key of a invoice must be unique!'),
        ]

    order_date = fields.Date('Order Date', states=_STATES)
    party_reference = fields.Char('Party Reference',
            states=_STATES)
    external_invoice_key = fields.Char('External Invoice Key')

    def _get_move_line(self, invoice, date, amount):
        '''
        Return move line
        '''
        res = super(Invoice, self)._get_move_line(invoice, date, amount)

        if invoice.party_reference:
            res['name'] = invoice.party_reference
        return res

Invoice()


