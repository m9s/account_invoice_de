# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Germany',
    'name_de_DE': 'Fakturierung Deutschland',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Add-Ons for Germany
    - Provides invoicing features typically required in Germany

      * Sets dependencies to required modules

      Contains:

      * Removes button for Pro Forma Invoice ?
      * Adds field 'Order Date' ?
      * Adds field 'Party Reference' ?
      * Adds field 'External Invoice Key' ?
      * Uses 'Party Reference' in _get_move_line ?
      * Invoice report ?

''',
    'description_de_DE': '''Erweiterungen für Deutschland
    - Stellt Erweiterungen für die Fakturierung zur Verfügung, die
      typischerweise in Deutschland benötigt werden.

      * Setzt Abhängigkeiten für benötigte Module

      Enthält:

      *

''',
    'depends': [
        #'account_batch',
        #'account_batch_invoice',
        #'account_batch_invoice_timeline',
        #'account_de',
        #'account_invoice',
        #'account_invoice_external_reference',
        #'account_invoice_history',
        #'account_invoice_product_rule',
        #'account_invoice_tax_rule_product_type',
        #'account_invoice_tax_rule_timeline',
        #'account_invoice_time_supply',
        #'account_move_invoice',
        #'account_party_open_items',
        #'account_timeline_invoice',
        #'account_timeline_invoice_product_rule',
        #'account_timeline_party_open_items',
        #'account_timeline_product_rule',
        #'account_timeline_tax_de',
        #'account_timeline_tax_de_datev',
    ],
    'xml': [
        # 'invoice.xml',
    ],
    'translation': [
        # 'locale/de_DE.po',
    ],
}
